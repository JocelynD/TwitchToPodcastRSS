"""
Cache the latest videos of a twitch channel

Only cache audios where recording is finished.

Outputs a RSS podcast stream XML on standard output
"""
import argparse
import dateutil
import datetime
import json
import logging
import math
import re
import typing

from twitchrss import (
    construct_archive_rss, fetch_channel, fetch_vods,
    fetch_streams, audio_cache, get_audiostream_fd
)


RE_HH_MM_TIME = re.compile(r'^(?P<hh>[0-2]?\d):(?P<mm>[0-5]?\d):(?P<ss>[0-5]?\d)$')


def hh_mm_time(s: str) -> typing.Tuple[int, int]:
    m = RE_HH_MM_TIME.match(s)
    format_error = argparse.ArgumentTypeError(f'"{s}" does not fit HH:MM:SS format.')
    if not m:
        raise format_error
    else:
        hours = int(m.group('hh'))
        minutes = int(m.group('mm'))
        seconds = int(m.group('ss'))

        if hours >= 23:
            raise format_error
        else:
            return hours, minutes, seconds


def seconds_to_hh_mm_ss(total_seconds):
    hours = math.floor(total_seconds/60/60)
    minutes = math.floor((total_seconds - hours*3600)/60)
    seconds = int(total_seconds - hours * 3600 - minutes * 60)
    return hours, minutes, seconds


def parse_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        'channel',
        help='channel (eg: "ostpolitik" for https://www.twitch.tv/ostpolitik)'
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        '--url-suffix',
        help='to be appended to every video URL (eg: "?t=0h10m00s" to skip the 10 first minutes)',
        default='',
    )
    group.add_argument(
        '--crop-start-at',
        help='Start at a fixed hour (UTC) (croping a waiting time for eg if the stream starts sooner)',
        type=hh_mm_time,
    )
    parser.add_argument(
        '--skip-audio-caching',
        help='Generate RSS feed but do not cache audio (may lead to deadlinks)',
        default=False,
        action='store_true',
    )
    parser.add_argument(
        '--cleanup',
        default=False,
        action='store_true',
    )
    return parser.parse_args()


def cache_channel(channel_name, url_suffix='', crop_start_at=None, skip_audio_caching=False, cleanup=False):
    """
    Cache the first (recentmost) page of a channel
    """
    user_data = json.loads(fetch_channel(channel_name))['data'][0]
    channel_id = user_data['id']
    vods_data = json.loads(fetch_vods(channel_id))['data']
    streams_data = json.loads(fetch_streams(channel_id))['data']

    if len(streams_data) > 0:
        current_stream_id = streams_data[0]['id']
        ended_vods_data = (i for i in vods_data if i['stream_id'] != current_stream_id)
    else:
        ended_vods_data = vods_data

    vod_ids_in_feed = []

    for video in ended_vods_data:
        video_id = video['id']

        if crop_start_at:

            duration_str = re.sub('[hm]', ':', video['duration']).replace('s', '')
            duration_tuple = tuple(
                int(i) for i in duration_str.split(':')
            )
            if len(duration_tuple) == 3:
                duration_td = datetime.timedelta(
                    hours=duration_tuple[0],
                    minutes=duration_tuple[1],
                    seconds=duration_tuple[2],
                )
            elif len(duration_tuple) == 2:
                duration_td = datetime.timedelta(
                    minutes=duration_tuple[0],
                    seconds=duration_tuple[1],
                )
            else:
                duration_td = datetime.timedelta(
                    seconds=duration_tuple[0],
                )

            start_at_hour, start_at_minute, start_at_second = crop_start_at
            original_start_dt = dateutil.parser.isoparse(video['published_at'])
            original_end_dt = original_start_dt + duration_td

            crop_start_dt = original_start_dt.replace(
                hour=start_at_hour,
                minute=start_at_minute,
                second=start_at_second,
            )

            if crop_start_dt < original_start_dt or crop_start_dt > original_end_dt:
                logging.warning(
                    f'Not cropping start at {crop_start_at}: '
                    f"outside of stream hours ({original_start_dt} → {original_end_dt})"
                )
            else:

                crop_start_td = crop_start_dt - original_start_dt
                # override url_suffix arg
                hours, minutes, seconds = seconds_to_hh_mm_ss(crop_start_td.total_seconds())
                url_suffix = f'?t={hours}h{minutes}m{seconds}s'

        video_url = video['url'] + url_suffix

        vod_ids_in_feed.append(video_id)

        if audio_cache.has(video_id):
            logging.debug(f'Video {video_id} already in cache')
        elif audio_cache.is_processing(video_id):
            logging.error(
                f'Temp file found for {video_id}, something went wrong during download/conversion ? '
                'You may want to delete manually and re-run script.'
            )
        else:
            if not skip_audio_caching:
                fd = get_audiostream_fd(video_url)
                audio_cache.store(video_id, fd)
            else:
                logging.warning(
                    f'{video_url} should be cached but --skip-audio-caching '
                    'is enabled. Produced RSS will have deadlinks.'
                )

        audio_cache.cleanup(
            vod_ids_to_keep=[video['id'] for video in ended_vods_data],
            dry_run=not cleanup,
        )

    return construct_archive_rss(user_data, ended_vods_data, url_suffix)



if __name__ == '__main__':
    args = parse_args()
    print(cache_channel(args.channel, args.url_suffix, args.crop_start_at, args.skip_audio_caching, args.cleanup).decode())
