"""
File: twitchrss.py
Author: Laszlo Zeke and Mattia Di Eleuterio
Github: https://github.com/madiele/TwitchToPodcastRSS
Description: webserver that converts a twitch channel into a podcast feed
"""

# Copyright 2020 Laszlo Zeke
# modifications: Copyright 2021 Mattia Di Eleuterio
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from html import escape as html_escape
from os import environ
import subprocess
from threading import Lock, RLock
from dateutil.parser import parse as parse_date
import copy
import datetime
import gzip
import json
import logging
import os
import re
import shutil
import time
import urllib

import pytz
from cachetools import cached, TTLCache
from feedgen.feed import FeedGenerator
from feedgen.ext.podcast import PodcastExtension
from feedgen.ext.podcast_entry import PodcastEntryExtension
from feedgen.util import xml_elem
from flask import abort, Flask, request, Response, render_template, send_file
from lxml.etree import CDATA
from ratelimit import limits, sleep_and_retry
from streamlink import Streamlink
from streamlink.exceptions import PluginError

VOD_URL_TEMPLATE = 'https://api.twitch.tv/helix/videos?sort=time&user_id=%s&type=all'
VOD_BY_ID_URL_TEMPLATE = 'https://api.twitch.tv/helix/videos?sort=time&id=%s&type=all'
USERID_URL_TEMPLATE = 'https://api.twitch.tv/helix/users?login=%s'
STREAMS_URL_TEMPLATE = 'https://api.twitch.tv/helix/streams?user_id=%s'
VODCACHE_LIFETIME = 10 * 60
USERIDCACHE_LIFETIME = 24 * 60 * 60
VODURLSCACHE_LIFETIME = 24 * 60 * 60
VODAUTHORIZED_LIFETIME = 24 * 60 * 60
MAX_DL_SIZE = 1 * 1024 * 1024 * 1024
CHANNEL_FILTER = re.compile("^[a-zA-Z0-9_]{2,25}$")
TWITCH_CLIENT_ID = environ.get("TWITCH_CLIENT_ID")
TWITCH_SECRET = environ.get("TWITCH_SECRET")
TWITCH_OAUTH_TOKEN = ""
TWITCH_OAUTH_EXPIRE_EPOCH = 0
URL_PREFIX = environ.get('URL_PREFIX')  # eg: "http://example.com:8081/foo"
WHITELISTED_CHANNELS = environ.get('WHITELISTED_CHANNELS', '').split(',')  # eg: "ostpolitik,truc"
AUDIO_CACHE_PATH = environ.get('AUDIO_CACHE_PATH', '/tmp/twitchrss_cache/')

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.DEBUG if environ.get('DEBUG') else logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S'
)


class PodcastExtensionWithFunding(PodcastExtension):
    def __init__(self):
        super().__init__()
        self.__podcast_funding = None

    def extend_ns(self):
        extensions = super().extend_ns()
        extensions.update({'podcast': 'https://podcastindex.org/namespace/1.0'})
        return extensions

    def extend_rss(self, rss_feed):
        channel = rss_feed[0]
        PODCASTINDEX_NS = 'https://podcastindex.org/namespace/1.0'

        if self.__podcast_funding:
            funding = xml_elem('{%s}funding' % PODCASTINDEX_NS, channel)
            funding.attrib['url'] = self.__podcast_funding['url']
            label = self.__podcast_funding['text']
            if label:
                funding.text = label

        return rss_feed

    def podcast_funding(self, url=None, text=None):
        if url:
            self.__podcast_funding = dict(url=url, text=text)
        else:
            return self.__podcast_funding



if not TWITCH_CLIENT_ID:
    raise Exception("Twitch API client id env variable is not set.")
if not TWITCH_SECRET:
    raise Exception("Twitch API secret env variable not set.")

app = Flask(__name__)
streamlink_session = Streamlink(options={'stream-segment-threads': 3})
streamUrl_queues = {}
cache_locks = {
    'fetch_channel': Lock(),
    'fetch_vods': Lock(),
    'fetch_streams': Lock(),
    'get_audiostream_url': Lock(),
    'check_vod_proxy_authorized': Lock(),
}


def authorize():
    """updates the oauth token if expired."""

    global TWITCH_OAUTH_TOKEN
    global TWITCH_OAUTH_EXPIRE_EPOCH

    if TWITCH_OAUTH_EXPIRE_EPOCH >= round(time.time()):
        return
    logging.debug("requesting a new oauth token")
    data = {
        'client_id': TWITCH_CLIENT_ID,
        'client_secret': TWITCH_SECRET,
        'grant_type': 'client_credentials',
    }
    url = 'https://id.twitch.tv/oauth2/token'
    request = urllib.request.Request(url, data=urllib.parse.urlencode(data).encode("utf-8"), method='POST')
    retries = 0
    while retries < 3:
        try:
            result = urllib.request.urlopen(request, timeout=3)
            r = json.loads(result.read().decode("utf-8"))
            TWITCH_OAUTH_TOKEN = r['access_token']
            TWITCH_OAUTH_EXPIRE_EPOCH = int(r['expires_in']) + round(time.time())
            logging.debug("oauth token aquired")
            return
        except Exception as e:
            logging.warning("Fetch exception caught: %s" % e)
            retries += 1
    logging.error("could not get oauth token from twitch")
    abort(503)


class NoAudioStreamException(Exception):
    """NoAudioStreamException."""
    pass


@cached(cache=TTLCache(maxsize=3000, ttl=VODAUTHORIZED_LIFETIME), lock=cache_locks['check_vod_proxy_authorized'])
def check_vod_proxy_authorized(vod_id):
    data = json.loads(fetch_vods(id_=vod_id))['data']
    if len(data) > 0:
        return data[0]['user_login'] in WHITELISTED_CHANNELS
    else:
        return True


def check_channel_proxy_authorized(channel_name):
    return channel_name in WHITELISTED_CHANNELS


@cached(cache=TTLCache(maxsize=3000, ttl=VODURLSCACHE_LIFETIME), lock=cache_locks['get_audiostream_url'])
def get_audiostream(vod_url):
    """finds the audio-strem URL for the given link and returns it.

    Args:
      vod_url: link to the vod
    Returns: the audio stream url

    """
    tries = 0;
    max_tries = 3;
    while tries < max_tries:
        tries = tries + 1
        try:
            vod = streamlink_session.streams(vod_url)

            if 'audio' in vod:
                audio_key = 'audio'
            elif 'audio_only' in vod:
                audio_key = 'audio_only'
            else:
                audio_key = None

            if audio_key is None:
                #TODO: cache the error in some way to prevent calling streamlink on the same vod
                #      so to reduce wasted api calls
                logging.debug(
                    f"the selected vod does not have an audio stream, available : {vod}"
                )
                raise NoAudioStreamException(
                    f"the selected vod does not have an audio stream, available : {vod} {vod_url}"
                )

            stream = vod.get(audio_key)
            return stream

        except PluginError as e:
            logging.error("streamlink has returned an error for url " + str(vod_url) + ":")
            logging.error(e)
            if tries >= max_tries:
                raise NoAudioStreamException("could not process the audio stream")

    raise NoAudioStreamException("could not get the audio stream for uknown reason")


def get_audiostream_url(vod_url):
    """finds the audio-strem URL for the given link and returns it.

    Args:
      vod_url: link to the vod
    Returns: the audio stream url

    """
    logging.debug("looking up audio url for " + vod_url)
    return get_audiostream(vod_url).to_url()


def get_audiostream_fd(vod_url):
    """finds the audio-strem URL for the given link and returns it.

    Args:
      vod_url: link to the vod
    Returns: the audio stream url

    """
    logging.debug("looking up audio url for " + vod_url)
    return get_audiostream(vod_url).open()


class AudioCache:
    def __init__(self, path):
        if not os.path.exists(path):
            os.mkdir(path)
        self.path = path

    def get_cache_path(self, vod_id):
        return self._mk_path(vod_id, 'm4a')

    def _mk_path(self, vod_id, extension):
        return os.path.join(self.path, f'{vod_id}.{extension}')

    def get_tmp_download_path(self, vod_id):
        return self._mk_path(vod_id, 'downloading.aac')

    def get_tmp_conversion_path(self, vod_id):
        return self._mk_path(vod_id, 'converting.m4a')

    def has(self, vod_id):
        return os.path.exists(self.get_cache_path(vod_id))

    def is_processing(self, vod_id):
        return (
            os.path.exists(self.get_tmp_download_path(vod_id))
            or
            os.path.exists(self.get_tmp_conversion_path(vod_id))
        )

    def _encapsulate(self, aac_path, m4a_path):
        """ Encapsulate AAC bitstream within mp4 container

        No re-encoding is performed
        """
        logging.debug(f"Converting {aac_path} to {m4a_path}…")
        cmd = ["ffmpeg", "-i", aac_path,"-c:a", "copy",  "-bsf:a", "aac_adtstoasc",  m4a_path]
        try:
            ret = subprocess.check_output(cmd)
        except subprocess.CalledProcessError as e:
            logger.error("ffmpeg called failed : {e}")
            raise


    def store(self, vod_id, stream_fd):
        """ Possibly overwriting existing entry
        """
        download_path = self.get_tmp_download_path(vod_id)
        convert_path = self.get_tmp_conversion_path(vod_id)
        cache_path = self.get_cache_path(vod_id)

        logging.info(f'Caching {vod_id}')
        logging.debug(f'Downloading into {download_path}…')
        with open(download_path, 'wb') as cache_fd:
            while True:
                written = cache_fd.write(stream_fd.read(MAX_DL_SIZE))
                if written == 0:
                    break
        logging.debug(f'Finished downloading {vod_id}…')
        logging.debug(f'Encapsulating {vod_id}…')
        self._encapsulate(download_path, convert_path)

        shutil.move(convert_path, cache_path)
        os.remove(download_path)

    def cleanup(self, vod_ids_to_keep, dry_run=False):
        vods_paths_to_keep = [
            self.get_cache_path(vod_id)
            for vod_id in vod_ids_to_keep
        ]
        vods_paths_in_dir = [
            os.path.join(self.path, filename)
            for filename in os.listdir(self.path)
            if filename.endswith('.m4a')
        ]

        for vod_path in vods_paths_in_dir:
            if vod_path in vods_paths_to_keep:
                msg = f"Keeping cached file {vod_path}"
                if dry_run:
                    logging.info(msg)
                else:
                    logging.debug(msg)
            else:
                if dry_run:
                    msg = f"Would delete {vod_path} (but cleanup is not enabled)"
                else:
                    msg = f"Deleting {vod_path} as it is no longer in the feed"
                logging.info(msg)
                if not dry_run:
                    os.remove(vod_path)


audio_cache = AudioCache(AUDIO_CACHE_PATH)


@app.route('/vod/<string:channel>', methods=['GET', 'HEAD'])
def vod(channel):
    """process request to /vod/.

    Args:
      channel: Returns: the http response

    Returns:

    """

    if CHANNEL_FILTER.match(channel):
        return process_channel(channel, request.args)
    else:
        abort(404)


@app.route('/vodonly/<string:channel>', methods=['GET', 'HEAD'])
def vodonly(channel):
    """process request to /vodonly/.

    Args:
      channel: Returns: the http response

    Returns:

    """
    if CHANNEL_FILTER.match(channel):
        return process_channel(channel, request.args)
    else:
        abort(404)


@app.route('/cached_video/<int:vod_id>', methods=['GET', 'HEAD'])
def cached_video(vod_id):
    return send_file(audio_cache.get_cache_path(vod_id))


def _stream_twitch_audio(vod_url, request):
    fd = get_audiostream(vod_url).open()

    def generate():
        while True:
            buf = fd.read(10 * 1024)  # 10ko chunks
            if len(buf) == 0:  # EOF
                break
            else:
                yield buf

    if request.method == 'GET':
        response = Response(generate())
    elif request.method == 'HEAD':
        response = Response()

    response.headers['Content-Type'] = 'audio/aac'
    return response


@app.route('/proxy_video/<int:vod_id>', methods=['GET', 'HEAD'])
def proxy_video(vod_id):
    if check_vod_proxy_authorized(vod_id):
        timestamp = request.args.get("t", None)

        vod_url = f'https://www.twitch.tv/videos/{vod_id}'
        if timestamp:
            vod_url += f'?t={timestamp}'

        return _stream_twitch_audio(vod_url, request)
    else:
        abort(403)


@app.route('/proxy_stream/<string:channel_name>', methods=['GET', 'HEAD'])
def proxy_stream(channel_name):
    if check_channel_proxy_authorized(channel_name):
        vod_url = f'https://www.twitch.tv/{channel_name}'
        return _stream_twitch_audio(vod_url, request)
    else:
        abort(403)


@app.route('/')
def index():
    """process request to the root."""
    return render_template('index.html')


def process_channel(channel, request_args):
    """process the given channel.

    Args:
      channel: the channel string given in the request
      request_args: the arguments of the http request

    Returns:
      rss_data: the fully formed rss feed

    """
    if not check_channel_proxy_authorized(channel):
        abort(403)

    include_streaming = True if request_args.get("include_streaming", "False").lower() == "true" else False
    sort_by = request_args.get("sort_by", "published_at").lower()
    desc = True if request_args.get("desc", "False").lower() == "true" else False
    links_only = True if request_args.get("links_only", "False").lower() == "true" else False

    try:
        user_data = json.loads(fetch_channel(channel))['data'][0]
        channel_id = user_data['id']
        vods_data = json.loads(fetch_vods(channel_id))['data']
        streams_data = json.loads(fetch_streams(channel_id))['data']
    except KeyError as e:
        logging.error("could not fetch data for the given request")
        logging.error(e)
        abort(404)

    rss_data = construct_rss(user_data, vods_data, streams_data, include_streaming, sort_by=sort_by, desc_sort=desc, links_only=links_only)
    headers = {'Content-Type': 'text/xml'}

    if 'gzip' in request.headers.get("Accept-Encoding", ''):
        headers['Content-Encoding'] = 'gzip'
        rss_data = gzip.compress(rss_data)

    return rss_data, headers


@cached(cache=TTLCache(maxsize=3000, ttl=USERIDCACHE_LIFETIME), lock=cache_locks['fetch_channel'])
def fetch_channel(channel_name):
    """fetches the JSON for the given channel username.

    Args:
      channel_name: the channel name

    Returns: the JSON formatted channel info

    """
    return fetch_json(channel_name, USERID_URL_TEMPLATE)


@cached(cache=TTLCache(maxsize=500, ttl=VODCACHE_LIFETIME), lock=cache_locks['fetch_vods'])
def fetch_vods(channel_id=None, id_=None):
    """fetches the JSON for the given channel username.

    Args:
      channel_id: the unique identifier of the channel
    Returns: the JSON formatted vods list

    """
    if channel_id:
        return fetch_json(channel_id, VOD_URL_TEMPLATE)
    elif id_:
        return fetch_json(id_, VOD_BY_ID_URL_TEMPLATE)
    else:
        raise ValueError()



@cached(cache=TTLCache(maxsize=500, ttl=VODCACHE_LIFETIME), lock=cache_locks['fetch_streams'])
def fetch_streams(user_id):
    """fetches the JSON formatted list of streams for the give user

    Args:
      user_id: the unique identifier of the channel

    Returns: the JSON formatted vods list

    """
    return fetch_json(user_id, STREAMS_URL_TEMPLATE)


def get_auth_headers():
    """gets the headers for the twitch API and requests a new oauth token if needed

    Returns: a dict containing auth data

    """
    authorize()
    return {
        'Authorization': 'Bearer ' + TWITCH_OAUTH_TOKEN,
        'Client-Id': TWITCH_CLIENT_ID,
    }


@sleep_and_retry
@limits(calls=800, period=60)
def fetch_json(id, url_template):
    """fetches a JSON from the given URL template and generic id.

    Args:
      id: the unique identifier of your request
      url_template: the template for the request where id will be replaced example: 'https://api.twitch.tv/helix/videos?user_id=%s&type=all'

    Returns: the JSON response for the request

    """
    url = url_template % id
    headers = get_auth_headers()
    headers['Accept-Encoding'] = 'gzip'
    request = urllib.request.Request(url, headers=headers)
    retries = 0
    while retries < 3:
        try:
            result = urllib.request.urlopen(request, timeout=3)
            logging.debug('Fetch from twitch for %s with code %s' % (id, result.getcode()))
            if result.info().get('Content-Encoding') == 'gzip':
                logging.debug('Fetched gzip content')
                return gzip.decompress(result.read())
            return result.read()
        except Exception as e:
            logging.warning("Fetch exception caught: %s" % e)
            retries += 1
    logging.error("max retries reached, could not get resource, id: " + id)
    abort(503)


def construct_rss(user, vods, streams, include_streams=False, sort_by="published_at", desc_sort=False, links_only=False):
    """returns the RSS for the given inputs.

    Args:
      user: the user dict
      vods: the vod dict
      streams: the streams dict
      include_streams: True if the streams should be included (Default value = False)
      sort_by: the key to sort by, the keys are the same used by the twitch API https://dev.twitch.tv/docs/api/reference#get-videos
      desc_sort: True if the sort must be done in ascending oreder
      links_only: if True the audio stream will not be fetched, makes the feed generation very fast

    Returns: fully formatted RSS string

    """

    logging.debug("processing channel")
    if links_only:
        logging.debug("links_only enabled, not fetching audio streams")
    try:
        channel_name = user['login']
        display_name = user['display_name']
        icon = user['profile_image_url']
        is_streaming = True if streams else False
    except KeyError as e:
        logging.error("error while processing user data")
        logging.error(e)
        abort(500)
    logging.debug("streaming=" + str(is_streaming))
    logging.debug("user data:")
    logging.debug(user)
    if is_streaming:
        logging.debug("streams data:")
        logging.debug(streams)

    feed = FeedGenerator()
    # feed.load_extension('podcast')
    feed.register_extension('podcast', PodcastExtensionWithFunding, PodcastEntryExtension)

    # Set the feed/channel level properties
    feed.image(url=icon)
    feed.id("https://github.com/madiele/TwitchToPodcastRSS")
    feed.title("La matinale d'Ost")
    feed.link(href='https://www.twitch.tv/' + channel_name, rel='self')
    feed.author(name=channel_name)
    feed.description("Matinalier sur Twitch depuis janvier 2020, je vous propose chaque matin de 7h à 9h une émission audio qui parle d'actualité française et internationale. Aussi au programme : des chroniques, des jeux et autres. Contact : MatinaleOstpolitik@protonmail.com")
    feed.podcast.itunes_author(channel_name)
    feed.podcast.itunes_complete(False)
    feed.podcast.itunes_explicit('no')
    feed.podcast.itunes_image(icon)
    feed.podcast.itunes_summary("La matinale d'Ost")
    feed.podcast.podcast_funding(
        url='https://utip.io/ostpolitik',
        text='Donner à ostpolitik via uTip',
    )

    # Create an item
    if vods:

        try:
            is_date = False
            try:
                parse_date(vods[0][sort_by])
                is_date = True
            except (ValueError, OverflowError, TypeError):
                is_date = False

            logging.debug("ordering by: " + str(sort_by) + "; desc_sort=" + str(desc_sort))
            if is_date:
                vods = sorted(vods, key=lambda kv: parse_date(kv[sort_by]), reverse=desc_sort)
            else:
                vods = sorted(vods, key=lambda kv: kv[sort_by], reverse=desc_sort)
        except KeyError:
            logging.error("can't order by " + sort_by + " resorting to ordering by id")
            sort_by = "published_at"
            try:
                vods = sorted(vods, key=lambda kv: kv['id'], reverse=False)
            except KeyError:
                logging.error("can't order by standard ordering")
        for vod in vods:
            try:

                logging.debug("processing vod:" + vod['id'])
                logging.debug(vod)
                description = ""
                is_vod_currently_streaming = (
                    is_streaming
                    and
                    vod['stream_id'] == streams[0]['id']
                )
                if is_vod_currently_streaming:
                    print('---------------')
                    import pprint;pprint.pprint(vod)
                    import pprint;pprint.pprint(streams)
                    print('---------------')
                    if not include_streams:
                        logging.debug("skipping incomplete vod")
                        continue
                    else:
                        description = "<p>Attention: ce stream est en cours d'enregistrement.</p>"
                        # thumb = "https://vod-secure.twitch.tv/_404/404_processing_320x180.png"
                        # For live streams, thumbnail is in stream object, not vod object
                        thumb = streams[0]['thumbnail_url'].format(width=512, height=288)
                else:
                    thumb = vod['thumbnail_url'].replace("%{width}", "512").replace("%{height}", "288")
                item = feed.add_entry()
                link = vod['url']

                description += "<a href=\"%s\"><p>%s</p><img src=\"%s\" /></a>" % (
                    link, html_escape(vod['title']), thumb)
                if vod['description']:
                    description += "<br/>" + vod['description']

                if not links_only:
                    # prevent get_audiostream_url to be run concurrenty with the same paramenter
                    # this way the next hit on get_audiostream_url will use the cache instead

                    global streamUrl_queues
                    if link not in streamUrl_queues:
                        q = streamUrl_queues[link] = {'lock': RLock(), 'count': 0}
                    else:
                        q = streamUrl_queues[link]

                    q['count'] = q['count'] + 1
                    q['lock'].acquire()

                    try:
                        stream = get_audiostream(link)

                    except NoAudioStreamException as e:
                        description += "TwitchToPodcastRSS ERROR: could not fetch an audio stream for this vod,"
                        description += "try refreshing the RSS feed later"
                        description += "<br>reason: " + str(e)
                        stream = None

                    q['count'] = q['count'] - 1
                    if q['count'] == 0:
                        del streamUrl_queues[link]

                    q['lock'].release()

                    duration_str = re.sub('[hm]', ':', vod['duration']).replace('s', '')
                    duration_tuple = tuple(
                        int(i) for i in duration_str.split(':')
                    )
                    if duration_tuple > (0, 10, 0):
                        # skip first minutes
                        stream_url_suffix = '?t=0h9m50s'
                    else:
                        stream_url_suffix = ''

                    if stream:
                        # https://www.twitch.tv/videos/1168774853
                        _, video_id = link.rsplit('/', 1)

                        audio_is_cached = audio_cache.has(video_id)
                        if audio_is_cached:
                            # archive
                            stream_url = f'{URL_PREFIX}/cached_video/{video_id}'

                        else:  # Currently steraming, or not cached yet
                            # Not realy a live because it will start from beggining
                            stream_url = f'{URL_PREFIX}/proxy_video/{video_id}{stream_url_suffix}'


                        item.enclosure(stream_url, type='audio/aac')

                description += '<br><br><p>Generated by TwitchToPodcastRSS </p>'
                item.link(href=link, rel="related")
                item.description(description)
                date = datetime.datetime.strptime(vod['created_at'], '%Y-%m-%dT%H:%M:%SZ')

                item.podcast.itunes_duration(duration_str)
                item.podcast.itunes_author(channel_name)
                if thumb.endswith('.jpg') or thumb.endswith('.png'):
                    item.podcast.itunes_image(thumb)

                item.pubDate(pytz.utc.localize(date))
                item.updated(pytz.utc.localize(date))
                if stream and 'proxy' in stream_url:
                    item.title('[LIVE RATTRAPAGE] ' + vod['title'])
                    guid = vod['id']# + '-proxy'
                    # Seek will not work, let's try to avoid it…
                    item.podcast.itunes_duration('0:0:0')
                else:
                    item.title(vod['title'])
                    guid = vod['id']
                item.guid(guid)

                if is_vod_currently_streaming:
                    # Add a LIVE copy
                    item_live = copy.copy(item)
                    stream_url = f'{URL_PREFIX}/proxylive/{channel_name}'
                    item_live.enclosure(stream_url, type='audio/aac')
                    item_live.title('[LIVE SYNCHRO] ' + vod['title'])
                    # Seek will not work, let's try to avoid it…
                    item.podcast.itunes_duration('0:0:0')
                    item_live.guid(guid+'-live')
                    feed.add_entry(item_live)
            except KeyError as e:
                logging.warning('Issue with json while processing vod: %s\n\nException: %s' % (vod, e))
                feed.remove_entry(item)

    logging.debug("all vods processed")
    return feed.rss_str()



def construct_archive_rss(user, vods, stream_url_suffix):
    """returns the RSS for the given inputs ; fully static (no live stream)

    Args:
      user: the user dict
      vods: the vod dict

    Returns: fully formatted RSS string

    """
    logging.debug("processing channel for static RSS archive")

    sort_by = "published_at"

    try:
        channel_name = user['login']
        display_name = user['display_name']
        icon = user['profile_image_url']
    except KeyError as e:
        logging.error("error while processing user data")
        logging.error(e)
        raise

    logging.debug("user data:")
    logging.debug(user)

    feed = FeedGenerator()
    feed.load_extension('podcast')
    feed.register_extension('podcastplus', PodcastExtensionWithFunding, PodcastEntryExtension)

    # Set the feed/channel level properties
    feed.image(url=icon)
    feed.title("La matinale d'Ost")
    feed.link(href='https://podcast.crapouillou.net/#ostpolitik', rel='self')
    feed.author(name=channel_name)
    feed.language("fr")
    feed.description(CDATA('<br />'.join((
        "Matinalier sur Twitch depuis janvier 2020, je vous propose chaque matin de 7h à 9h une émission audio qui parle d'actualité française et internationale. Aussi au programme : des chroniques, des jeux et autres. ",
        "Contact : MatinaleOstpolitik@protonmail.com <br />",
        "Ce flux podcast non officiel est mis à jour quotidiennement, entre 9h et 9h30. Un mois d'archives sont conservées en ligne.",
    ))))
    feed.podcast.itunes_author(channel_name)
    feed.podcast.itunes_complete(False)
    feed.podcast.itunes_explicit('no')
    feed.podcast.itunes_image(icon)
    feed.podcast.itunes_summary("La matinale d'Ost")
    feed.podcast.itunes_category({"cat": "News", "sub": "Politics"})
    feed.podcastplus.podcast_funding(
        url='https://utip.io/ostpolitik',
        text='Donner à ostpolitik via uTip',
    )
    # Create an item
    if vods:
        try:
            vods = sorted(vods, key=lambda kv: parse_date(kv[sort_by]), reverse=False)
        except KeyError:
            logging.error("can't order by " + sort_by + " resorting to ordering by id")
            try:
                vods = sorted(vods, key=lambda kv: kv['id'], reverse=False)
            except KeyError:
                logging.error("can't order by standard ordering")
        for vod in vods:
            try:

                logging.debug("processing vod:" + vod['id'])

                logging.debug(vod)
                thumb = vod['thumbnail_url'].replace("%{width}", "512").replace("%{height}", "288")
                item = feed.add_entry()
                link = vod['url']

                description = "<a href=\"%s\"><p>%s</p><img src=\"%s\" /></a>" % (
                    link, html_escape(vod['title']), thumb)
                if vod['description']:
                    description += "<br/>" + vod['description']

                # prevent get_audiostream_url to be run concurrenty with the same paramenter
                # this way the next hit on get_audiostream_url will use the cache instead

                try:
                    stream = get_audiostream(link)

                except NoAudioStreamException as e:
                    description += "TwitchToPodcastRSS ERROR: could not fetch an audio stream for this vod,"
                    description += "try refreshing the RSS feed later"
                    description += "<br>reason: " + str(e)
                    stream = None

                duration_str = re.sub('[hm]', ':', vod['duration']).replace('s', '')

                if stream:
                    # https://www.twitch.tv/videos/1168774853
                    _, video_id = link.rsplit('/', 1)

                    cached_media_url  = f'{URL_PREFIX}/cached_video/{video_id}'

                    item.enclosure(cached_media_url, type='audio/aac')

                item.link(href=link, rel="related")
                item.description(description)
                date = datetime.datetime.strptime(vod['created_at'], '%Y-%m-%dT%H:%M:%SZ')

                item.podcast.itunes_duration(duration_str)
                item.podcast.itunes_author(channel_name)
                if thumb.endswith('.jpg') or thumb.endswith('.png'):
                    item.podcast.itunes_image(thumb)

                item.pubDate(pytz.utc.localize(date))
                item.updated(pytz.utc.localize(date))

                item.title(vod['title'])
                guid = vod['id']
                item.guid(guid)

            except KeyError as e:
                logging.warning('Issue with json while processing vod: %s\n\nException: %s' % (vod, e))
                feed.remove_entry(item)

    logging.debug("all vods processed")
    return feed.rss_str()


# For debug
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8081, debug=True)
