# Flux podcast audio pour la matinale d'Ost

*Based on [TwitchToPodcastRSS](https://github.com/madiele/TwitchToPodcastRSS) by
Mattia Di Eleuterio, but heavily hacked a non-generic way.*

[« La matinale d'Ost »](https://www.twitch.tv/ostpolitik) est une émission
matinale diffusée en vidéo sur twitch.tv, réalisée par ostpolitik:

> Matinalier sur Twitch depuis janvier 2020, je vous propose chaque matin de 7h
> à 9h une émission audio qui parle d'actualité française et
> internationale. Aussi au programme : des chroniques, des jeux et
> autres. Contact : MatinaleOstpolitik@protonmail.com

Ce programme vise à en publier une version audio seulement (des archives, soit uniquement les épisodes dont l'enregistrement est terminé), :

- sous forme d'un podcast « standard »
- pouvant s'ouvrir dans [AntenaPod](https://antennapod.org/)

(peut-être qu'un jour les flux live seront disponibles également, mais ça n'est pas trivial…)

# Installation

*NB: un [rôle ansible](rôle ansible(https://framagit.org/jocelynd/twitchtopodcastrss) existe aussi, mais fait un usage purement statique de TwitchToPodcastRSS.*

(le dockerfile n'est pas suffisant)

sudo apt install g++ gcc libxml2 libpcre3-dev libxslt-dev python3-dev python3-lxml python3-pip zlib1g-dev
python3 -m venv venv
./venv/bin/pip install -r requirements.txt

# Lancement

## Le script

Il sert à mettre en cache les fichiers pour éviter de les streamer depuis
twitch à chaque fois. Quand les fichiers (.aac) sont en cache, il est possible
de se déplacer dans le fichier (seek), ce qui n'est pas supporté sinon.

Il récupère une channel twitch à la fois.

Le flux podcast XML est également retourné sur la sortie standard.

```
URL_PREFIX='https://mydomain.tld'
AUDIO_CACHE_PATH=/var/cache/twitch_podcast
TWITCH_SECRET="sicrete" 
TWITCH_CLIENT_ID="sicrete" 
./cache_channel.py ostpolitik
```

